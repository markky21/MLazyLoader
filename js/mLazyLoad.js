var MLazyLoad;

(function () {

    "use strict";

    MLazyLoad = function (img, options) {

        if (!(this instanceof MLazyLoad)) {
            return new MLazyLoad(img, options);
        }

        this._img = img;
        this._imgClone = img.cloneNode();
        this._config = this._extendOptions(options);
        this._isLoaded = false;

        this._init();

    };

    MLazyLoad.prototype._defaultOptions = {
        loader: '',
        fadeInOut: true,
        animateWidth: false,
        animateHeight: false,
        callback: null
    };

    MLazyLoad.prototype._extendOptions = function (options) {
        var defaultConfig = JSON.parse(JSON.stringify(this._defaultOptions));

        for (var key in defaultConfig) {
            if (key in options) {
                defaultConfig[key] = options[key];
            }
        }

        return defaultConfig;
    };

    MLazyLoad.prototype._init = function () {
        this._downloadImg();
        if (!this._isLoaded) {
            this._img.setAttribute('src', this._config.loader);
        }
    };

    MLazyLoad.prototype._downloadImg = function () {

        this._imgClone.onload = function () {
            this._isLoaded = true;

            if (this._config.fadeInOut) {
                this._animateImg(function () {
                    this._replaceNode();
                }.bind(this));
            } else {
                this._replaceNode();
            }

        }.bind(this);
    };

    MLazyLoad.prototype._replaceNode = function () {
        this._img.parentNode.replaceChild(this._imgClone, this._img);
        if (typeof this._config.callback === 'function') {
            this._config.callback();
        }
    };

    MLazyLoad.prototype._animateImg = function (callback) {

        Object.assign(this._img.style, {
            transition: 'all 0.3s ease',
            opacity: 0
        });
        Object.assign(this._imgClone.style, {
            transition: 'all 0.3s ease',
            opacity: 0
        });
        if (this._config.animateWidth) {
            Object.assign(this._imgClone.style, {
                maxWidth: this._img.clientWidth + 'px',
                width: '100%'
            });
        }
        if(this._config.animateHeight) {
            Object.assign(this._imgClone.style, {
                maxHeight: this._img.clientHeight + 'px',
                objectFit: 'contain'
            });
        }

        setTimeout(function () {
            callback();
            setTimeout(function () {
                this._imgClone.style.opacity = '1';
                if (this._config.animateWidth) {
                    this._imgClone.style.maxWidth = '10000px';
                }
                if (this._config.animateHeight) {
                    this._imgClone.style.maxHeight = '10000px';
                }
            }.bind(this), 100);
        }.bind(this), 300);
    };


})();

